class User < ApplicationRecord
  has_many :microposts
  validates :name, presence: true
  validates :email, presence:true

  def last_post
    'broken'
  end
end
